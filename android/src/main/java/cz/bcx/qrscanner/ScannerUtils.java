package cz.bcx.qrscanner;

import android.annotation.TargetApi;
import android.hardware.Camera;
import android.os.Build;
import android.util.Size;

import java.util.List;

public class ScannerUtils {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Size getOptimalSize(int requestedWidth, int requestedHeight, Size[] availableSizes) {
        Size size = null;
        long sizeResolution = 0;

        long requestedSizeResolution = requestedWidth * requestedHeight;

        for (Size s : availableSizes) {
            long resolution = s.getWidth() * (long) s.getHeight();

            if ((size == null) || //Save first size
                resolution >= requestedSizeResolution && resolution < sizeResolution
            ) {
                size = s; // Save if s fits our needs better
                sizeResolution = resolution;
            }
        }

        return size;
    }

    public static Camera.Size getOptimalSize(int requestedWidth, int requestedHeight, List<Camera.Size> availableSizes){
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)requestedHeight / requestedWidth;

        if (availableSizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        for (Camera.Size size : availableSizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - requestedHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - requestedHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : availableSizes) {
                if (Math.abs(size.height - requestedHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - requestedHeight);
                }
            }
        }
        return optimalSize;
    }

}
