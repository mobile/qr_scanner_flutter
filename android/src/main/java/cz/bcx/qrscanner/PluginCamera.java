package cz.bcx.qrscanner;

public interface PluginCamera {
    void startPreview() throws Exception;
    void stopPreview() throws Exception;
    void enableScanning();
    void disableScanning();
    void dispose();

}
