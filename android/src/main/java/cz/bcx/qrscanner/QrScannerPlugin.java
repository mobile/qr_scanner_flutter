package cz.bcx.qrscanner;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.view.OrientationEventListener;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.FlutterView;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class QrScannerPlugin implements MethodCallHandler, PluginRegistry.RequestPermissionsResultListener {
  private static final int CAMERA_REQUEST_ID = 77_1337_77;

  private PluginCamera camera;
  private OrientationEventListener orientationEventListener;
  private int lastRotation = -1;

  private Registrar registrar;
  private FlutterView view;
  private Activity activity;

  private static MethodChannel methodChannel;
  private EventChannel eventChannel;
  private EventChannel.EventSink eventSink;

  private Runnable initializeTask = null;

  private QrScannerPlugin(Registrar registrar, FlutterView view, final Activity activity) {
    this.registrar = registrar;
    this.view = view;
    this.activity = activity;

    this.registrar.addRequestPermissionsResultListener(this);

  }

  public static void registerWith(Registrar registrar) {
    QrScannerPlugin.methodChannel = new MethodChannel(registrar.messenger(), "cz.bcx.qr_scanner");
    QrScannerPlugin.methodChannel.setMethodCallHandler(new QrScannerPlugin(registrar, registrar.view(), registrar.activity()));
  }

  /**
   * Available methods: ["initialize", "startPreview", "stopPreview", "enableScanning", "disableScanning", "dispose"]
   * @param call - Method call from Flutter
   * @param result - Object to let Flutter know result of the method call.
   */
  @Override
  public void onMethodCall(MethodCall call, Result result) {
    if(call.method.equals("initialize")) {
      if(!(call.arguments instanceof Map)) {
        throw new IllegalArgumentException("Call's arguments is not instance of a Map.");
      }

      Map<String, Object> arguments = (Map<String, Object>) call.arguments;

      String quality = (String) arguments.get("previewQuality");
      double captureWidth = (double) arguments.get("previewWidth");
      double captureHeight = (double) arguments.get("previewHeight");

      PreviewQuality previewQuality = PreviewQuality.getPreviewQualityForName(quality);

      onInitialize(previewQuality, result, captureWidth, captureHeight);
    }
    else if(call.method.equals("startPreview")) {
      onStartPreview();
      result.success(null);
    }
    else if(call.method.equals("stopPreview")) {
      onStopPreview();
      result.success(null);

    }
    else if(call.method.equals("enableScanning")) {
      onEnableScanning();
      result.success(null);
    }
    else if(call.method.equals("disableScanning")) {
      onDisableScanning();
      result.success(null);
    }
    else if(call.method.equals("dispose")) {
      onDispose();
      result.success(null);
    }
    else {
      result.notImplemented();
    }
  }

  private OrientationEventListener getOrientationEventListener() {
    return new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {

      @Override
      public void onOrientationChanged(int orientation) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        if (rotation != lastRotation) {

          Map<String, Object> event = new HashMap<>();
          event.put("eventType", "orientationChange");
          event.put("orientation", rotation);
          eventSink.success(event);
          lastRotation = rotation;
        }
      }
    };
  }

  private boolean hasCameraPermissions() {
    return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
            activity.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public boolean onRequestPermissionsResult(int id, String[] strings, int[] ints) {
    if (id == CAMERA_REQUEST_ID) {
      initializeTask.run();
      return true;
    }
    return false;
  }

  private void onInitialize(final PreviewQuality previewQuality, final Result result, final double captureWidth, final double captureHeight) {
    if(initializeTask != null) {
      result.success(null); //TODO - We are waiting for permissions
      return;
    }

    if(orientationEventListener == null){
      orientationEventListener = getOrientationEventListener();
    }

    initializeTask = new Runnable() {
      @Override
      public void run() {
        try {
          // Initialize eventChannel and eventSink.
          if(QrScannerPlugin.this.eventChannel == null) {
            QrScannerPlugin.this.eventChannel = new EventChannel(
                    registrar.messenger(),
                    "cz.bcx.qr_scanner/events"
            );

            QrScannerPlugin.this.eventChannel.setStreamHandler(
                new EventChannel.StreamHandler() {
                  @Override
                  public void onListen(Object o, EventChannel.EventSink eventSink) {
                    QrScannerPlugin.this.eventSink = eventSink;
                  }

                  @Override
                  public void onCancel(Object o) {
                    QrScannerPlugin.this.eventSink = null;
                  }
                }
            );
          }

          int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
          CameraStateListener cameraStateListener = new CameraStateListener() {
            @Override
            public void onCameraDisconnected() {
              if (eventSink != null) {
                Map<String, String> event = new HashMap<>();
                event.put("eventType", "error");
                event.put("errorMessage", "The camera2 has been disconnected.");
                eventSink.success(event);
              }
            }

            @Override
            public void onCameraError(Camera2.CameraStateError cameraStateError) {
              if (eventSink != null) {
                Map<String, String> event = new HashMap<>();
                event.put("eventType", "error");
                event.put("errorMessage", cameraStateError.getMessage());
                eventSink.success(event);
              }
            }

            @Override
            public void onCodeScanned(String code) {
              if (eventSink != null && code != null) {
                Map<String, String> event = new HashMap<>();
                event.put("eventType", "codeScanned");
                event.put("code", code);
                eventSink.success(event);
              }
            }
          };

          boolean cameraNative = isCamera2Native();

          if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && cameraNative) {
            CameraManager cameraManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
            Camera2 camera2 = Camera2.createCameraInstance(cameraManager, view, previewQuality, (int) captureWidth, (int) captureHeight, activity.getWindowManager().getDefaultDisplay().getRotation());
            camera2.openCamera(cameraManager, result, rotation, cameraStateListener);
            QrScannerPlugin.this.camera = camera2;

          }else{

            QrScannerPlugin.this.camera = Camera1.createCameraInstance(view, previewQuality, result, (int) captureWidth, (int) captureHeight, rotation, cameraStateListener, !cameraNative & Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
          }

          initializeTask = null;
        } catch (Exception e) {
          result.error("CameraAccessException", "Exception raised when initializing qr scanner plugin.", e);
        }
      }
    };

    if(!hasCameraPermissions()) {
      activity.requestPermissions(new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST_ID);
    }
    else {
       initializeTask.run();
    }
  }

  private boolean isCamera2Native() {
    try {
      if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {return false;}
      CameraManager cameraManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
      String mCameraId = cameraManager.getCameraIdList()[0];
      CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(mCameraId);
      //CHECK CAMERA HARDWARE LEVEL. IF CAMERA2 IS NOT NATIVELY SUPPORTED, GO BACK TO CAMERA1
      Integer deviceLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
      return deviceLevel != null && (deviceLevel != CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY);
    }
    catch (CameraAccessException ex) {return false;}
    catch (NullPointerException e) {return false;}
    catch (ArrayIndexOutOfBoundsException ez) {return false;}
  }

  private void onStartPreview() {
    // TODO - Error Handling
    try {
      if (orientationEventListener == null){
        orientationEventListener = getOrientationEventListener();
      }
      orientationEventListener.enable();
      camera.startPreview();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void onStopPreview() {
    // TODO - Error Handling
    try {
      orientationEventListener.disable();
      camera.stopPreview();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void onEnableScanning() {
    // TODO - Error Handling
    camera.enableScanning();
  }

  private void onDisableScanning() {
    // TODO - Error Handling
    camera.disableScanning();
  }

  private void onDispose() {
    // TODO - Error handling
    orientationEventListener.disable();
    orientationEventListener = null;

    camera.dispose();
    camera = null;
  }
}