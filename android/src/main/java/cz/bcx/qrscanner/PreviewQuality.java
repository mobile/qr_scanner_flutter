package cz.bcx.qrscanner;

public enum PreviewQuality {
    LOW("low", 480, 320),
    MEDIUM("medium", 640, 480),
    HIGH("high", 1024, 768);

    private static final PreviewQuality FALLBACK_VALUE = PreviewQuality.MEDIUM;

    private final String serializedName;
    private final int previewWidth;
    private final int previewHeight;

    PreviewQuality(String serializedName, int previewWidth, int previewHeight ) {
        this.serializedName =serializedName;
        this.previewWidth = previewWidth;
        this.previewHeight = previewHeight;
    }

    protected static PreviewQuality getPreviewQualityForName(String serializedName) {
        for(PreviewQuality quality : values()) {
            if(quality.serializedName.equals(serializedName)) return quality;
        }

        return FALLBACK_VALUE;
    }

    public int getPreviewWidth() {
        return previewWidth;
    }

    public int getPreviewHeight() {
        return previewHeight;
    }
}