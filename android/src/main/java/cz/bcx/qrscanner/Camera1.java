package cz.bcx.qrscanner;

import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.AsyncTask;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.util.Arrays;
import java.util.HashMap;

import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class Camera1 implements PluginCamera {
    private static final int BETWEEN_SCANS_DELAY = 333; //ms

    private CameraStateListener cameraStateListener;

    private MultiFormatReader qrReader;

    private SurfaceTexture surfaceTexture;
    private Camera camera;

    private boolean scanningEnabled = false;
    private long lastTimeScanned = 0;

    private Camera1(final Camera camera, final CameraStateListener stateListener, FlutterView.SurfaceTextureEntry textureEntry, Camera.Size captureSize){
        this.camera = camera;
        this.cameraStateListener = stateListener;

        this.surfaceTexture = textureEntry.surfaceTexture();
        surfaceTexture.setDefaultBufferSize(captureSize.width, captureSize.height);

        this.qrReader = new MultiFormatReader();

        HashMap<DecodeHintType, Object> hints = new HashMap<>();
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Arrays.asList(BarcodeFormat.QR_CODE, BarcodeFormat.AZTEC, BarcodeFormat.PDF_417, BarcodeFormat.DATA_MATRIX));
        this.qrReader.setHints(hints);
    }

    public static Camera1 createCameraInstance(FlutterView flutterView, PreviewQuality previewQuality, final MethodChannel.Result result, int captureWidth, int captureHeight, int rotation, final CameraStateListener cameraStateListener, final boolean legacy) throws Exception {
        int cameraId = -1;

        for(int i = 0; i < Camera.getNumberOfCameras(); i++){
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            cameraId = i;
            if(info.facing == Camera.CameraInfo.CAMERA_FACING_BACK){
                break;
            }
        }


        if(cameraId == -1){
            //TODO replace to camera exception for camera first
            result.error("Camera", "Couldn't find any useable back-facing camera.", null);
            throw new Exception("Couldn't find any useable back-facing camera.");
        }


        FlutterView.SurfaceTextureEntry surfaceTextureEntry = flutterView.createSurfaceTexture();

        Camera camera = Camera.open(cameraId);
        Camera.Parameters parameters = camera.getParameters();


        Camera.Size previewSize = ScannerUtils.getOptimalSize(previewQuality.getPreviewWidth(), previewQuality.getPreviewHeight(), parameters.getSupportedPreviewSizes());
        Camera.Size captureSize = ScannerUtils.getOptimalSize(captureWidth, captureHeight, parameters.getSupportedPreviewSizes());

        HashMap<String, Object> response = new HashMap<>();
        response.put("textureId", cameraId);
        response.put("previewWidth", previewSize.width);
        response.put("previewHeight", previewSize.height);
        response.put("orientation", rotation);
        response.put("legacy", legacy);
        result.success(response);

        if(legacy) {
            parameters.setPreviewFormat(ImageFormat.NV21);
            parameters.setRotation(rotation);
        }

        parameters.setPictureSize(previewSize.width, previewSize.height);
        parameters.setPreviewSize(captureSize.width, captureSize.height);

        if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        }
        camera.setParameters(parameters);

        return new Camera1(camera, cameraStateListener, surfaceTextureEntry, captureSize);
    }

    @Override
    public void startPreview() throws Exception {
        camera.setPreviewTexture(surfaceTexture);

        camera.setPreviewCallback(new Camera.PreviewCallback() {
            @Override
            public void onPreviewFrame(byte[] data, Camera camera) {

                if (data == null || !scanningEnabled || System.currentTimeMillis() - lastTimeScanned < BETWEEN_SCANS_DELAY) {
                    return;
                }

                Camera.Size previewSize = camera.getParameters().getPreviewSize();

                new QrTask(data, previewSize).execute();

                lastTimeScanned = System.currentTimeMillis();

            }
        });

        camera.startPreview();
    }

    @Override
    public void stopPreview() throws Exception {
        camera.setPreviewTexture(null);
        camera.stopPreview();
    }

    @Override
    public void enableScanning() {
        scanningEnabled = true;
    }

    @Override
    public void disableScanning() {
        scanningEnabled = false;
    }

    @Override
    public void dispose() {
        if(camera != null){
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    private class QrTask extends AsyncTask<Void, Void, String>{
        private byte[] data;
        private Camera.Size previewSize;


        QrTask(byte[] data, Camera.Size previewSize){
            this.data = data;
            this.previewSize = previewSize;
        }

        @Override
        protected String doInBackground(Void... voids) {
            int previewWidth = previewSize.width;
            int previewHeight = previewSize.height;

            String resultCode = null;

            try {
                LuminanceSource source = new PlanarYUVLuminanceSource(
                        data,
                        previewWidth,
                        previewHeight,
                        0,
                        0,
                        previewWidth,
                        previewHeight,
                        false
                );

                BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                Result result = qrReader.decodeWithState(bitmap);

                resultCode = result.toString();
            } catch (Exception e){
                //Ignore
            } finally {
                qrReader.reset();
            }

            return resultCode;
        }

        @Override
        protected void onPostExecute(String code) {
            if(code == null || code.isEmpty() || cameraStateListener == null){
                return;
            }
            cameraStateListener.onCodeScanned(code);
        }
    }
}
