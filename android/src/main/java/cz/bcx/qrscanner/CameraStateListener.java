package cz.bcx.qrscanner;

public interface CameraStateListener {
    void onCameraDisconnected();
    void onCameraError(Camera2.CameraStateError cameraStateError);
    void onCodeScanned(String code);
}
