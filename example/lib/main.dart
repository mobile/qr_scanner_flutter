import 'package:flutter/material.dart';
import 'package:qr_scanner/qr_scanner.dart';
import 'package:qr_scanner_example/camera_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new HomePage()
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Color grayBackground = new Color.fromARGB(255, 134, 135, 137);
  final Color mojeIdYellow = new Color.fromARGB(255, 253, 204, 0);


  String qrCode;
  bool qrScanned = false;

  _codeScanned(String code){
    if(code.isNotEmpty){
      setState(() {
        qrCode = code;
        qrScanned = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    TextStyle codeText = Theme.of(context).textTheme.title.copyWith(color: Colors.white);

    return new Scaffold(
      backgroundColor: grayBackground,
      body: new Center(
          child: new Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
            margin: const EdgeInsets.all(20.0),
            decoration: new BoxDecoration(
                color: mojeIdYellow,
                borderRadius: const BorderRadius.all(const Radius.circular(5.0))
            ),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Text(qrScanned ? qrCode : "None", style: codeText,),
                new Padding(padding: const EdgeInsets.only(top: 40.0),
                  child: new RaisedButton(onPressed: ()=> Navigator.push(context, new MaterialPageRoute(builder: (context)=>new CameraPage(codeScanned: _codeScanned))),
                    child: new Text("SCAN"),
                  ),
                )
              ],
            ),
        )
      )
    );
  }
}
