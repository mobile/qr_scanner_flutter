import 'package:flutter/material.dart';
import 'package:qr_scanner/qr_scanner.dart';

class CameraPage extends StatefulWidget {
  final Function(String) codeScanned;
  CameraPage({Key key, this.codeScanned}) : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {


  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: new AppBar(
        title: new Text("Camera"),
      ),
      body: new Container(
        width: screenSize.width,
        height: screenSize.height,
        child: ScannerPreview(
          codeScanned: (code){
            widget.codeScanned(code);
            Navigator.pop(context);
          },
          width: screenSize.width,
          height: screenSize.height,
          stoppable: true,
        ),
      ),
    );
  }
}
