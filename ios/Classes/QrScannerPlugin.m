#import "QrScannerPlugin.h"
#import <AVFoundation/AVFoundation.h>
#import <libkern/OSAtomic.h>

@interface NSError (FlutterError)
@property(readonly, nonatomic) FlutterError *flutterError;
@end

@implementation NSError (FlutterError)
- (FlutterError *)flutterError {
    return [FlutterError errorWithCode:[NSString stringWithFormat:@"Error %d", (int)self.code]
                               message:self.domain
                               details:self.localizedDescription];
}
@end

@interface QRCam : NSObject<FlutterTexture, AVCaptureMetadataOutputObjectsDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, FlutterStreamHandler>
@property(readonly, nonatomic) int64_t textureId;
@property(readonly, nonatomic) AVCaptureSession *captureSession;
@property(readonly, nonatomic) AVCaptureDevice *captureDevice;
@property(readonly, nonatomic) AVCaptureDeviceInput *captureDeviceInput;
@property(readonly, nonatomic) AVCaptureMetadataOutput *captureMetadataOutput;
@property(readonly, nonatomic) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property(readonly, nonatomic) AVCaptureVideoDataOutput *captureVideoOutput;
@property(readonly, nonatomic) AVCaptureConnection *connection;
@property(readonly, nonatomic) int lastOrientation;
@property(readonly, nonatomic) CGSize previewSize;
@property(readonly) CVPixelBufferRef volatile latestPixelBuffer;

@property(nonatomic, copy) void (^onFrameAvailable)();

@property(nonatomic) FlutterEventChannel *eventChannel;
@property(nonatomic) FlutterEventSink eventSink;
@property(nonatomic) BOOL enableScanning;


- (instancetype)initCamera: (NSString *)resolutionPreset
                            error:(NSError **)error;
- (void)start;
- (void)stop;


@end

@implementation QRCam

-(instancetype)initCamera:  (NSString *)resolutionPreset
                            error:(NSError **)error{
    self = [super init];
    _enableScanning = NO;
    
    _captureSession = [[AVCaptureSession alloc] init];
    AVCaptureSessionPreset preset;
    if ([resolutionPreset isEqualToString:@"high"]) {
        preset = AVCaptureSessionPresetHigh;
    } else if ([resolutionPreset isEqualToString:@"medium"]) {
        preset = AVCaptureSessionPresetMedium;
    } else {
        NSAssert([resolutionPreset isEqualToString:@"low"], @"Unknown resolution preset %@",
                 resolutionPreset);
        preset = AVCaptureSessionPresetLow;
    }
    
    _captureSession.sessionPreset = preset;
    _captureDevice = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    NSError *localError = nil;
    _captureDeviceInput =
    [AVCaptureDeviceInput deviceInputWithDevice:_captureDevice error:&localError];
    if (localError) {
        *error = localError;
        return nil;
    }
    
    CMVideoDimensions dimensions =
    CMVideoFormatDescriptionGetDimensions([[_captureDevice activeFormat] formatDescription]);
    _previewSize = CGSizeMake(dimensions.width, dimensions.height);
    
    _captureVideoOutput = [AVCaptureVideoDataOutput new];
    _captureVideoOutput.videoSettings =
    @{(NSString *)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA) };
    [_captureVideoOutput setAlwaysDiscardsLateVideoFrames:YES];
    [_captureVideoOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    _connection =
    [AVCaptureConnection connectionWithInputPorts:_captureDeviceInput.ports
                                           output:_captureVideoOutput];
    if ([_captureDevice position] == AVCaptureDevicePositionFront) {
        _connection.videoMirrored = YES;
    }
    [_captureSession addInputWithNoConnections:_captureDeviceInput];
    [_captureSession addOutputWithNoConnections:_captureVideoOutput];
    [_captureSession addConnection:_connection];
    
    _captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput: _captureMetadataOutput];
    [_captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    [_captureMetadataOutput setMetadataObjectTypes:[self barcodeTypes]];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged)
     name:UIDeviceOrientationDidChangeNotification
     object:nil];
    
    return self;
}


- (void) orientationChanged
{
    if(_eventSink == nil)
        return;
    
    int orientation = [self getOrientation];
    if(orientation != -1 && _lastOrientation != orientation){
        _eventSink(@{
                     @"eventType" : @"orientationChange",
                     @"orientation" : @(orientation),
                     });
        _lastOrientation = orientation;
    }
}

- (int)getOrientation {
    switch ([UIApplication sharedApplication].statusBarOrientation) {
        case UIInterfaceOrientationPortrait:
            return 1;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            return 3;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            return 2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            return 0;
            break;
        case UIInterfaceOrientationUnknown:
        default:
            return -1;
            break;
    }
}

- (int) numberByDeviceOrientation: (UIDeviceOrientation) orientation{
    
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            return 0;
        case UIDeviceOrientationLandscapeLeft:
            return 1;
        case UIDeviceOrientationPortraitUpsideDown:
            return 2;
        case UIDeviceOrientationLandscapeRight:
            return 3;
     
            
        default:
            return -1;
    }
    
    
}




- (NSArray*)barcodeTypes {
    NSMutableArray *metadataObjectTypes = [NSMutableArray array];
    [metadataObjectTypes addObject:AVMetadataObjectTypeQRCode];
    [metadataObjectTypes addObject:AVMetadataObjectTypeAztecCode];
    [metadataObjectTypes addObject:AVMetadataObjectTypePDF417Code];
    [metadataObjectTypes addObject:AVMetadataObjectTypeDataMatrixCode];
    
    return [NSArray arrayWithArray:metadataObjectTypes];
    
}

- (CVPixelBufferRef)copyPixelBuffer {
    CVPixelBufferRef pixelBuffer = _latestPixelBuffer;
    while (!OSAtomicCompareAndSwapPtrBarrier(pixelBuffer, nil, (void **)&_latestPixelBuffer)) {
        pixelBuffer = _latestPixelBuffer;
    }
    

    return pixelBuffer;
}

- (void)dealloc {
    if (_latestPixelBuffer) {
        CFRelease(_latestPixelBuffer);
    }
}
#pragma mark Video data capture

- (void)captureOutput:(AVCaptureOutput *)output
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    if(output == _captureVideoOutput){
        CVPixelBufferRef newBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        CFRetain(newBuffer);
        CVPixelBufferRef old = _latestPixelBuffer;
        while (!OSAtomicCompareAndSwapPtrBarrier(old, newBuffer, (void **)&_latestPixelBuffer)) {
            old = _latestPixelBuffer;
        }
        if (old != nil) {
            CFRelease(old);
        }
        if (_onFrameAvailable)
        {
            _onFrameAvailable();
        }
    }

    if (!CMSampleBufferDataIsReady(sampleBuffer)) {
        _eventSink(@{
                     @"eventType" : @"error",
                     @"errorMessage" : @"sample buffer is not ready. Skipping sample"
                     });
        return;
    }

}


#pragma mark Metadata capture

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection {
    

    if(_enableScanning == YES && metadataObjects != nil && [metadataObjects count] > 0){
        AVMetadataObject *data = metadataObjects[0];
        NSString *code = [(AVMetadataMachineReadableCodeObject *)data stringValue];
    
        _eventSink(@{
                     @"eventType" : @"codeScanned",
                     @"code" : code
                     });
    }
}

- (FlutterError *_Nullable)onCancelWithArguments:(id _Nullable)arguments {
    _eventSink = nil;
    return nil;
}

- (FlutterError *_Nullable)onListenWithArguments:(id _Nullable)arguments
                                       eventSink:(nonnull FlutterEventSink)events {
    _eventSink = events;
    return nil;
}

- (void)start {
    [_captureSession startRunning];
}

- (void)stop {
    [_captureSession stopRunning];
}

- (void)enable{
    _enableScanning = YES;
}

- (void)disable{
    _enableScanning = NO;
}

- (void)dispose{
    [_captureSession stopRunning];
    for (AVCaptureInput *input in [_captureSession inputs]) {
        [_captureSession removeInput:input];
    }
    for (AVCaptureOutput *output in [_captureSession outputs]) {
        [_captureSession removeOutput:output];
    }
    
}

@end

@interface QrScannerPlugin ()
@property(readonly, nonatomic) NSObject<FlutterTextureRegistry> *registry;
@property(readonly, nonatomic) NSObject<FlutterBinaryMessenger> *messenger;
@property(readonly, nonatomic) int64_t texture;
@property(readonly, nonatomic) QRCam *camera;
@property(nonatomic) FlutterEventChannel *eventChannel;
@end

@implementation QrScannerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel *channel = [FlutterMethodChannel
                                     methodChannelWithName: @"cz.bcx.qr_scanner"
                                     binaryMessenger: [registrar messenger]];
    
    
    QrScannerPlugin *instance = [[QrScannerPlugin alloc] initWithRegistry:[registrar textures] messenger:[registrar messenger]];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)initWithRegistry:(NSObject<FlutterTextureRegistry> *)registry
                        messenger:(NSObject<FlutterBinaryMessenger> *)messenger
                        {
    self = [super init];
    NSAssert(self, @"super init cannot be nil");
    _registry = registry;
    _messenger = messenger;
    
    return self;
}

- (void)handleMethodCall:(FlutterMethodCall *)call result:(FlutterResult)result {
    if([@"initialize" isEqualToString:call.method]){
        if(![[call.arguments class] isSubclassOfClass:[NSMutableDictionary class]]){
            NSLog(@"Call's arguments is not instance of a Map.");
            return;
        }
        
        NSMutableDictionary *arg = (NSMutableDictionary *)call.arguments;
        NSString *quality = [arg objectForKey:@"previewQuality"];
        
        NSError *error;
        QRCam *cam = [[QRCam alloc] initCamera: quality
                                error: &error];
        
        if(error){
            result([error flutterError]);
            return;
        }
        
        if(_camera){
            [_camera dispose];
        }
        int64_t textureId = [_registry registerTexture:cam];
        _texture = textureId;
        _camera = cam;
        
        cam.onFrameAvailable = ^{
            [_registry textureFrameAvailable:textureId];
        };
        self.eventChannel = [FlutterEventChannel
                                             eventChannelWithName:[NSString
                                                                   stringWithFormat:@"cz.bcx.qr_scanner/events",
                                                                   textureId]
                                             binaryMessenger:_messenger];
        [self.eventChannel setStreamHandler:cam];
        cam.eventChannel = self.eventChannel;
        result(@{
                 @"textureId" : @(textureId),
                 @"previewWidth" : @(cam.previewSize.width),
                 @"previewHeight" : @(cam.previewSize.height),
                 @"orientation" : @(1)
                 });
        
        
    }else if([@"startPreview" isEqualToString:call.method]){
        
        [_camera start];
        result(nil);
        
    }else if([@"stopPreview" isEqualToString:call.method]){
        
        [_camera stop];
        result(nil);
        
    }else if([@"enableScanning" isEqualToString:call.method]){
        
        [_camera enable];
        result(nil);
        
    }else if([@"disableScanning" isEqualToString:call.method]){
        
        [_camera disable];
        result(nil);
        
    }else if([@"dispose" isEqualToString:call.method]){
        [_camera dispose];
        [_registry unregisterTexture:_texture];
        
        result(nil);
        
    }else {
        result(FlutterMethodNotImplemented);
    }
}
@end
