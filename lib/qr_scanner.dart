import 'dart:async';

import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

final MethodChannel _channel =
    const MethodChannel('cz.bcx.qr_scanner');

class ScannerPreview extends StatefulWidget {
  Function(String) codeScanned;
  PreviewQuality previewQuality;

  double width;
  double height;
  Alignment stopButtonAlign;
  bool stoppable;
  Color progressColor;
  double progressWidth;

  ScannerPreview({
    Key key,
    @required this.codeScanned,
    this.previewQuality = PreviewQuality.high,
    this.width = 800.0,
    this.height = 600.0,
    this.stoppable = false,
    this.stopButtonAlign = Alignment.bottomCenter,
    this.progressColor = Colors.black,
    this.progressWidth = 5.0
  }) : super(key : key);

  @override
  _ScannerPreviewState createState() => _ScannerPreviewState();
}

class _ScannerPreviewState extends State<ScannerPreview> with WidgetsBindingObserver{

  ScannerController controller;

  String error;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    this.controller = new ScannerController(
      previewQuality: widget.previewQuality,
      previewWidth: widget.width,
      previewHeight: widget.height,
      onCodeScanned: _scanned,
      orientationChange: (){
          setState(() {

          });
      } ,
    );
    initController();
  }

  initController() async {
    try {
      controller.addListener(() {
        if(controller.value.error != null) {
          controller.stopPreview();
          error = controller.value.error;
        }

        if(!controller.value.disposed) {
          setState(() {}); //Update state
        }
      });

      await controller.initialize();
      controller.enableScanning();
      controller.startPreview();

    } on ScannerException catch(e) {
      print(e.message);
    }
  }

  _togglePreview() {
    if(controller.value.previewStarted)
      controller.stopPreview();
    else
      controller.startPreview();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(this.controller != null && this.controller.value.initialized) {
      switch (state.index) {
        case 0:
          if(!this.controller.value.previewStarted) this.controller.startPreview(); //onResume
          break;
        case 2:
          if(this.controller.value.previewStarted) this.controller.stopPreview(); //onPause
          break;
      }
    }
  }

  @override
  void dispose() {
    print("dispose was called");
    controller.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  _scanned(String code){
    controller.stopPreview();
    if(widget.codeScanned != null){
      widget.codeScanned(code);
    }
  }

  Widget _getTexture(){
    if(this.controller != null && this.controller.value.initialized) {
      int textureOrientation;

      if(!controller.value.legacy) {
        switch (controller.value.orientation) {
          case 1:
            textureOrientation = Platform.isAndroid ? 3 : 1;
            break;
          case 3:
            textureOrientation = Platform.isAndroid ? 1 : 3;

            break;
          default:
            textureOrientation = controller.value.orientation;
            break;
        }
      }else{
        switch (controller.value.orientation) {
          case 0:
            textureOrientation = 1;
            break;
          case 1:
            textureOrientation = 0;
            break;
          case 2:
            textureOrientation = 3;
            break;
          case 3:
            textureOrientation = 2;
            break;
          default:
            textureOrientation = controller.value.orientation;
            break;
        }

      }


      return new RotatedBox(
        quarterTurns: textureOrientation,
        child: new SizedBox(
          child: new Texture(textureId: controller.textureId),
        ),
      );
    }else
      return new Center(
        child: new CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(widget.progressColor), strokeWidth: widget.progressWidth,),
      );
  }

  @override
  Widget build(BuildContext context) {
    bool swap = (controller.value.orientation == 1 || controller.value.orientation == 3);
    return new Container(
        width: swap ? widget.height : widget.width,
        height: swap ? widget.width : widget.height,
        child: widget.stoppable ? new Stack(
            children: <Widget>[
              _getTexture(),
              new Container( //Bottom control
                alignment: widget.stopButtonAlign,
                padding: const EdgeInsets.only(bottom: 8.0),
                child: new GestureDetector(
                    onTap: _togglePreview,
                    child: new SizedBox.fromSize(
                      size: new Size(48.0, 48.0),
                      child: new Container(
                        decoration: new ShapeDecoration(
                            shadows: [
                              new BoxShadow(
                                  color: const Color(0x66000000),
                                  blurRadius: 8.0,
                                  spreadRadius: 4.0
                              )
                            ],
                            shape: new CircleBorder(
                                side: new BorderSide(
                                    color: Colors.white,
                                    width: 2.0
                                )
                            )
                        ),
                        child: new Center(
                          child: new Icon(
                            this.controller.value.previewStarted ? Icons.stop : Icons.play_arrow,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                ),
              ),
            ]
        ) : _getTexture(),
      );
  }
}


enum PreviewQuality {
  low,    // 320x240
  medium, // 640x480
  high    // 1024x768
}

String _serializePreviewQuality(PreviewQuality previewQuality) {
  switch(previewQuality) {
    case PreviewQuality.low:
      return 'low';
    case PreviewQuality.medium:
      return 'medium';
    case PreviewQuality.high:
      return 'high';

    // Fallback to medium quality
    default:
      return 'medium';
  }
}

class ScannerValue {
  final bool initialized;

  final bool previewStarted;
  final Size previewSize;
  final bool scanningEnabled;

  final String error;

  final bool disposed;
  final int orientation;

  final bool legacy;

  const ScannerValue({
    this.initialized,
    this.previewStarted,
    this.previewSize,
    this.scanningEnabled,
    this.error,
    this.disposed,
    this.orientation,
    this.legacy
  });

  const ScannerValue.uninitialized() : this(initialized: false, previewStarted: false, scanningEnabled: false, disposed: false, orientation: 0, legacy: false);

  ScannerValue copyWith({
    bool initialized,
    bool previewStarted,
    Size previewSize,
    bool scanningEnabled,
    String error,
    bool disposed,
    int orientation,
    bool legacy
  }) {
    return new ScannerValue(
      initialized: initialized ?? this.initialized,
      previewStarted: previewStarted ?? this.previewStarted,
      previewSize: previewSize ?? this.previewSize,
      scanningEnabled: scanningEnabled ?? this.scanningEnabled,
      error: error ?? this.error,
      disposed: disposed ?? this.disposed,
      orientation: orientation ?? this.orientation,
      legacy: legacy ?? this.legacy
    );
  }

  @override
  String toString() {
    return  '$runtimeType('
            'initialized: $initialized, '
            'previewStarted: $previewStarted, '
            'previewSize: $previewSize, '
            'scanningEnabled: $scanningEnabled)';
  }
}

class ScannerController extends ValueNotifier<ScannerValue> {
  Completer<Null> _initializeCompleter;

  PreviewQuality previewQuality;

  double previewWidth;
  double previewHeight;

  int _textureId;

  StreamSubscription _eventChannelSubscription;

  Function onCodeScanned;
  Function orientationChange;

  ScannerController({
    this.previewQuality : PreviewQuality.high,
    this.previewWidth,
    this.previewHeight ,
    this.onCodeScanned,
    this.orientationChange,
  }) : super(new ScannerValue.uninitialized());


  int get textureId => _textureId;


  Future<Null> initialize() async {
    if(value.disposed) {
      throw new ScannerException(message: "This instace of ScannerController has already been disposed. Please make a new one.");
    }

    try {
      _initializeCompleter = new Completer<Null>();

      final Map<dynamic, dynamic> methodResult = await _channel.invokeMethod(
        'initialize',
        <String, dynamic> {
          'previewQuality' : _serializePreviewQuality(previewQuality),
          'previewWidth' : previewWidth,
          'previewHeight' : previewHeight
        }
      );

      _textureId = methodResult['textureId'];
      value = value.copyWith(
        previewSize: new Size(
          methodResult['previewWidth'].toDouble(),
          methodResult['previewHeight'].toDouble(),
        ),
        orientation:  methodResult['orientation'],
        legacy: Platform.isAndroid ? methodResult['legacy'] : false
      );

      // Subscription to receive state and error events from native platform
      _eventChannelSubscription = new EventChannel('cz.bcx.qr_scanner/events')
          .receiveBroadcastStream()
          .listen(_onEventReceived);


      value = value.copyWith(
        initialized: true
      );

      _initializeCompleter.complete(null);
      return _initializeCompleter.future;
    } on PlatformException catch(e) {
      throw new ScannerException(message: "Failed while initializing ScannerController.", cause: e);
    }
  }

  void _onEventReceived(dynamic e) {
    if(value.disposed) return;

    try {
      Map<dynamic, dynamic> event = e;

      var eventType = event["eventType"];

      switch (eventType) {
        case "error":
          value = value.copyWith(error: event["errorMessage"]);
          break;
        case "orientationChange":
          value = value.copyWith(orientation: event["orientation"]);
          orientationChange();
          break;

        case "codeScanned":
        // Call onCodeScanned callback with code as a parameter
          if (onCodeScanned != null) {
            onCodeScanned(event["code"]);
          }
          break;
      }
    }catch (Exception){
      print(e.toString());
    }
  }

  void startPreview() async {
    if(value.initialized && !value.disposed) { //Error if not initialized or disposed
      await _channel.invokeMethod(
        'startPreview',
      );

      value = value.copyWith(previewStarted: true);
    }
  }

  void stopPreview() async {
    if(value.initialized && !value.disposed) { //Error if not initialized or disposed
      await _channel.invokeMethod(
        'stopPreview'
      );

      value = value.copyWith(previewStarted: false);
    }
  }

  void enableScanning() async {
    if(value.initialized && !value.disposed) { //Error if not initialized or disposed
      await _channel.invokeMethod(
          'enableScanning'
      );

      value = value.copyWith(scanningEnabled: true);
    }
  }

  void disableScanning() async {
    if(value.initialized && !value.disposed) { //Error if not initialized or disposed
      await _channel.invokeMethod(
          'disableScanning'
      );

      value = value.copyWith(scanningEnabled: false);
    }
  }

  @override
  Future<Null> dispose() {
    if(value.disposed) {
      return new Future<Null>.value(null);
    }

    value = value.copyWith(initialized: false, disposed: true);

    if(_initializeCompleter != null) {
      return _initializeCompleter.future.then((param) async {
        await _channel.invokeMethod(
          'dispose'
        );

        await _eventChannelSubscription.cancel();

        super.dispose();
      });
    }
    else {
      super.dispose();
      return new Future<Null>.value(null);
    }
  }
}

class ScannerException implements Exception {
  String message;
  Exception cause;

  ScannerException({this.message, this.cause});
}